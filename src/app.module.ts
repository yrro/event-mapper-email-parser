import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { EventMapperModule } from './event-mapper/event-mapper.module';
import { EmailParserModule } from './email-parser/email-parser.module';

@Module({
  imports: [PrismaModule, EventMapperModule, EmailParserModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
