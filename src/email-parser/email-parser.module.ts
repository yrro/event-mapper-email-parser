import { Module } from '@nestjs/common';
import { EmailParserController } from './controller/email-parser.controller';
import { PrismaModule } from '../prisma/prisma.module';
import { EmailParserService } from './service/email-parser.service';
import { PrismaService } from '../prisma/prisma.service';

@Module({
  controllers: [EmailParserController],
  providers: [EmailParserService, PrismaService],
  imports: [PrismaModule],
})
export class EmailParserModule {}
