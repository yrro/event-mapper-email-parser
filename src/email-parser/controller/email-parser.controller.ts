import {
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  HttpCode,
  HttpStatus,
  HttpException,
  BadRequestException,
  UploadedFile,
  UseInterceptors,
  Body,
} from '@nestjs/common';
import {
  ApiConsumes,
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { EmailParserService } from '../service/email-parser.service';
import { EmailResponseDto } from '../dto/response-dto/email-response.dto';
import { EmailRequestDto } from '../dto/request-dto/email-request.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('email')
@Controller('email')
export class EmailParserController {
  constructor(private readonly _emailParserService: EmailParserService) {}

  @Post('parser')
  @ApiCreatedResponse({ type: EmailResponseDto })
  @ApiOperation({ summary: 'Extract JSON from email' })
  @UsePipes(new ValidationPipe({ transform: true })) // Validate the incoming DTO
  @HttpCode(HttpStatus.CREATED)
  @ApiResponse({
    status: 201,
    description: 'JSON data successfully extracted',
    type: EmailResponseDto,
  })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @ApiResponse({ status: 500, description: 'Internal server error' })
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  async getJsonFromEmail(
    @UploadedFile() file: Express.Multer.File,
    @Body() EmailDto: EmailRequestDto,
  ): Promise<EmailResponseDto> {
    try {
      if (!file && !EmailDto.url) {
        throw new BadRequestException(
          'Either a file or a URL must be provided',
        );
      }

      if (file) {
        return this._emailParserService.getJsonFromEmailFile(file);
      } else if (EmailDto) {
        return this._emailParserService.getJsonFromUrl(EmailDto);
      } else {
        throw new BadRequestException(
          'The file path or JSON data source is required',
        );
      }
    } catch (error) {
      throw new HttpException(
        error.message || 'Failed to get the JSON from the provided source',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
