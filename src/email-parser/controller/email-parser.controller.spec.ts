import { Test, TestingModule } from '@nestjs/testing';
import { EmailParserController } from './email-parser.controller';
import { EmailParserService } from '../service/email-parser.service';
import { EmailResponseDto } from '../dto/response-dto/email-response.dto';
import { EmailRequestDto } from '../dto/request-dto/email-request.dto';
import {
  BadRequestException,
  HttpException,
  HttpStatus,
} from '@nestjs/common';

describe('EmailParserController', () => {
  let controller: EmailParserController;
  let service: EmailParserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EmailParserController],
      providers: [
        {
          provide: EmailParserService,
          useValue: {
            getJsonFromEmailFile: jest.fn(),
            getJsonFromUrl: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<EmailParserController>(EmailParserController);
    service = module.get<EmailParserService>(EmailParserService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('getJsonFromEmail', () => {

    it('should return JSON data from URL when URL is provided', async () => {
      const mockEmailDto: EmailRequestDto = { url: 'https://example.com/json' };
      const expectedResponse: EmailResponseDto = { data: {} };

      jest
        .spyOn(service, 'getJsonFromUrl')
        .mockResolvedValueOnce(expectedResponse);

      const result = await controller.getJsonFromEmail(undefined, mockEmailDto);

      expect(result).toEqual(expectedResponse);
      expect(service.getJsonFromUrl).toHaveBeenCalledWith(mockEmailDto);
    });

    it('should throw BadRequestException if neither file nor URL is provided', async () => {
      await expect(controller.getJsonFromEmail(undefined, {} as EmailRequestDto))
        .rejects.toThrow(new BadRequestException(  'Either a file or a URL must be provided'));
    });

    it('should throw HttpException with internal server error if service method throws error', async () => {
      jest.spyOn(service, 'getJsonFromEmailFile')
        .mockRejectedValueOnce(new HttpException(  'Failed to get the JSON from the provided source',
        HttpStatus.INTERNAL_SERVER_ERROR));

      await expect(controller.getJsonFromEmail({} as Express.Multer.File, {} as EmailRequestDto))
        .rejects.toThrow(HttpException);
    });
  });
});
