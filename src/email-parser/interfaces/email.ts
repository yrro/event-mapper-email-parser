import { Attachment } from 'mailparser';

// Defining the structure of the parsed email
export interface IParsedEmail {
  attachments: Attachment[];
  text?: string;
  html?: string;
}

// Defining the structure of the email attachment
export interface IEmailAttachment extends Attachment {
  contentType: string;
  content: Buffer;
}
