import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma/prisma.service';
import { EmailParserController } from './controller/email-parser.controller';
import { EmailParserModule } from './email-parser.module';
import { EmailParserService } from './service/email-parser.service';

describe('EmailParserModule', () => {
  let module: EmailParserModule;
  let controller: EmailParserController;
  let service: EmailParserService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const testingModule: TestingModule = await Test.createTestingModule({
      imports: [EmailParserModule],
    }).compile();

    module = testingModule.get<EmailParserModule>(EmailParserModule);
    controller = testingModule.get<EmailParserController>(
      EmailParserController,
    );
    service = testingModule.get<EmailParserService>(EmailParserService);
    prismaService = testingModule.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(module).toBeDefined();
  });

  it('should have EmailParserController', () => {
    expect(controller).toBeDefined();
  });

  it('should have EmailParserService', () => {
    expect(service).toBeDefined();
  });

  it('should have PrismaService', () => {
    expect(prismaService).toBeDefined();
  });
});
