import { ApiProperty } from '@nestjs/swagger';
import { IsObject } from 'class-validator';
import { JsonContent } from '../../../helpers/types/json-content';

export class EmailResponseDto {
  @ApiProperty({ description: 'The extracted JSON data' })
  @IsObject()
  data: JsonContent;
}
