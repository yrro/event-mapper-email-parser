import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class EmailRequestDto {
  @ApiProperty({
    required: false,
    description: 'URL - Link - Endpoint data source to extract JSON',
  })
  @IsString()
  @IsOptional()
  url?: string;

  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: false,
    description: 'Email file with a JSON link or attachment',
  })
  @IsString()
  @IsOptional()
  file?: Express.Multer.File;
}
