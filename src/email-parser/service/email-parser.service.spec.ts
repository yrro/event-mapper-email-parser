import { Test, TestingModule } from '@nestjs/testing';
import { EmailParserService } from './email-parser.service';
import { PrismaService } from '../../prisma/prisma.service';
import {
  NotFoundException
} from '@nestjs/common';

class PrismaServiceMock {
  async emailCreate(data: any): Promise<any> {
    return { data };
  }
}

describe('EmailParserService', () => {
  let service: EmailParserService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EmailParserService,
        { provide: PrismaService, useClass: PrismaServiceMock },
      ],
    }).compile();

    service = module.get<EmailParserService>(EmailParserService);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getDataFromParsedEmail', () => {
    it('should throw NotFoundException when no JSON content found', async () => {
      const parsedEmail = { attachments: [] };

      await expect(
        service['getDataFromParsedEmail'](parsedEmail),
      ).rejects.toThrow(NotFoundException);
    });
  });
});