import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Attachment } from 'mailparser';
import { PrismaService } from '../../prisma/prisma.service';
import { IParsedEmail } from '../interfaces/email';
import { EmailResponseDto } from '../dto/response-dto/email-response.dto';

import { parseEmailContent } from '../../helpers/parseEmailContent/parse-email-content';
import { JsonContent } from '../../helpers/types/json-content';
import { EmailRequestDto } from '../dto/request-dto/email-request.dto';
import { Prisma } from '@prisma/client';
import {
  getDataFromEmailBody,
  getDataFromEmailBodyLink,
} from '../../helpers/getDataFromEmailBody/getDataFromEmailBody';
import { getJsonFormURL } from '../../helpers/getJsonFromUrl/getJsonFromUrl';

@Injectable()
export class EmailParserService {
  constructor(private readonly _prismaService: PrismaService) {}

  // Extract JSON from provided URL
  async getJsonFromUrl(url: EmailRequestDto): Promise<EmailResponseDto> {
    const jsonContent = await getJsonFormURL(url.url);

    return await this.insertEmailData(jsonContent);
  }

  async getJsonFromEmailFile(
    file: Express.Multer.File,
  ): Promise<EmailResponseDto> {
    // Convert email to a UTF-8 format so we can identify its content
    const emailContent = file.buffer.toString('utf-8');

    // Parse Email content into a better format using the IParsedEmail interface
    const parsedEmail = await parseEmailContent(emailContent);

    /**
     * Extracting the JSON data from email based on the provided conditions
     * - As a file attachment
     * - Inside of the body of the email as a link/url
     * - Inside of the body of the email as a link/url that leads to a webpage
     *   and where there's the link that leads to the actual JSON
     */
    return await this.getDataFromParsedEmail(parsedEmail);
  }

  // Method to extract JSON from an already parsed email
  private async getDataFromParsedEmail(
    parsedEmail: IParsedEmail,
  ): Promise<EmailResponseDto> {
    try {
      let jsonContent: JsonContent | null = null;

      const emailAttachment = parsedEmail.attachments.find(
        (attachment: Attachment) =>
          attachment.contentType === 'application/json',
      );

      // Making sure there's at least an attachment file(s) and returning it
      if (emailAttachment) {
        jsonContent = JSON.parse(emailAttachment.content.toString());
      }

      // Verifying the email body for JSON Link
      if (!jsonContent) {
        jsonContent = await getDataFromEmailBody(parsedEmail);
      }

      // If still no JSON content found, check for a link that leads to a JSON link ...
      if (!jsonContent) {
        // Navigating to the webpage that has the link - Then hitting that link to extract the JSON data
        jsonContent = await getDataFromEmailBodyLink(parsedEmail);
      }

      // Nothing found - exception returned as response
      if (!jsonContent) {
        throw new NotFoundException('JSON not found in email file');
      }

      // Storing the value into the DB and returning it
      return await this.insertEmailData(jsonContent);
    } catch (error) {
      throw new NotFoundException(
        'Failed to get data from JSON.',
        error,
      );
    }
  }

  // Simple Database insertion
  private async insertEmailData(
    jsonContent: Prisma.JsonValue,
  ): Promise<EmailResponseDto> {
    return await this._prismaService.email.create({
      data: { data: jsonContent },
    });
  }
}
