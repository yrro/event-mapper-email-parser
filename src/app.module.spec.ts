import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import { PrismaModule } from './prisma/prisma.module';
import { EventMapperModule } from './event-mapper/event-mapper.module';

describe('AppModule', () => {
  let app: TestingModule;

  beforeEach(async () => {
    app = await Test.createTestingModule({
      imports: [AppModule, PrismaModule, EventMapperModule],
    }).compile();
  });

  afterEach(async () => {
    await app.close();
  });

  it('should be defined', () => {
    expect(app).toBeDefined();
  });
});
