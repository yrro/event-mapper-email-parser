import { Prisma } from '@prisma/client';

// Define a generic type for JSON content
export type JsonContent = Prisma.JsonValue;
