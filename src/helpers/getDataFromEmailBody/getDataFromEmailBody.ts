import axios from 'axios';
import { IParsedEmail } from '../../email-parser/interfaces/email';
import { getJsonFormURL } from '../getJsonFromUrl/getJsonFromUrl';
import { JsonContent } from '../types/json-content';

export async function getDataFromEmailBody(
  parsedEmail: IParsedEmail,
): Promise<JsonContent | null> {
  // Method to extract the json link from the email body
  const body = parsedEmail.html || parsedEmail.text || '';

  //Regex to match only the URL part and handle potential HTML tags
  const jsonLink = /https?:\/\/[^\s"]+\.json/;
  const jsonMatchLink = body.match(jsonLink);
  if (jsonMatchLink) {
    const jsonUrl = jsonMatchLink[0].split('">')[0]; // Ensure only the URL is taken
    return await getJsonFormURL(jsonUrl);
  }
}

// Method to extract the json from url that leads to the webpage and there's the json
export async function getDataFromEmailBodyLink(
  parsedEmail: IParsedEmail,
): Promise<JsonContent | null> {
  //Regex to match only the URL part and handle potential HTML tags
  const webPageLink = /https?:\/\/[^\s">]+/g;

  //Regex to match only the JSON URL
  const jsonLink = /https?:\/\/[^\s"]+\.json/;
  const linkMatch = (parsedEmail.html || parsedEmail.text || '')
    .match(webPageLink)[0]
    .split('">');

  if (linkMatch) {
    for (const link of linkMatch) {
      const intermediatePage = await axios.get<string>(link);

      const jsonLinkInPage = intermediatePage.data.match(jsonLink);

      if (jsonLinkInPage) {
        return await getJsonFormURL(jsonLinkInPage[0]);
      }
    }
  }
}
