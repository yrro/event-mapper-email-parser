import { simpleParser } from 'mailparser';
import { IParsedEmail } from '../../email-parser/interfaces/email';

export async function parseEmailContent(
  emailContent: string,
): Promise<IParsedEmail> {
  // Using simpleParser method to convert an email message into structured object.
  const parsedEmail = await simpleParser(emailContent);
  return parsedEmail as IParsedEmail;
}
