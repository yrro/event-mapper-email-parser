import axios from 'axios';
import { JsonContent } from '../types/json-content';

// Generic method to extract all data from provided URL
export async function getJsonFormURL(url: string): Promise<JsonContent | null> {
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    console.error(`Error fetching JSON from URL: ${url}`, error);
    return null;
  }
}
