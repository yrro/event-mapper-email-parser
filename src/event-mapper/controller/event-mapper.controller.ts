import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  HttpCode,
  HttpStatus,
  HttpException,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { EventMapperService } from '../service/event-mapper.service';
import { OutputEventDto } from '../dto/output-event/output-event.dto';
import { InputEventDto } from '../dto/input-event/input-event.dto';

@Controller('event')
@ApiTags('event')
export class EventMapperController {
  constructor(private readonly _eventMapperService: EventMapperService) {}

  @Post('mapper')
  @ApiCreatedResponse({ type: OutputEventDto })
  @UsePipes(new ValidationPipe({ transform: true })) // Validate the incoming DTO
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({
    summary: 'Mapping JSON Object following certain input structure.',
  })
  @ApiResponse({
    status: 201,
    description: 'The JSON has been successfully mapped.',
    type: [OutputEventDto],
  })
  @ApiResponse({ status: 500, description: 'Failed to map JSON' })
  eventMapper(@Body() inputEvent: InputEventDto): Promise<OutputEventDto[]> {
    try {
      const mappedEvent = this._eventMapperService.mapToMappedEvent(inputEvent);
      return mappedEvent;
    } catch (error) {
      throw new HttpException(
        error.message || 'Failed to map the provided JSON',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
