import { Test, TestingModule } from '@nestjs/testing';
import { EventMapperController } from './event-mapper.controller';
import { EventMapperService } from '../service/event-mapper.service';
import { InputEventDto } from '../dto/input-event/input-event.dto';
import { OutputEventDto } from '../dto/output-event/output-event.dto';
import { HttpException, HttpStatus } from '@nestjs/common';

describe('EventMapperController', () => {
  let controller: EventMapperController;
  let service: EventMapperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EventMapperController],
      providers: [
        {
          provide: EventMapperService,
          useValue: {
            mapToMappedEvent: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<EventMapperController>(EventMapperController);
    service = module.get<EventMapperService>(EventMapperService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('eventMapper', () => {
    it('should return mapped event data when the mapping is successful', async () => {
      const inputEvent: InputEventDto = {
        Records: [
          {
            ses: {
              mail: {
                timestamp: '2022-01-01T00:00:00Z',
                source: 'sender@example.com',
                destination: ['recipient@example.com'],
                messageId: '',
                headersTruncated: false,
                headers: [],
                commonHeaders: {
                  from: ['sender@example.com'],
                  to: ['recipient@example.com'],
                  subject: 'Test Subject',
                  returnPath: '',
                  date: '',
                  messageId: '',
                },
              },
              receipt: {
                spamVerdict: { status: 'PASS' },
                virusVerdict: { status: 'PASS' },
                spfVerdict: { status: 'PASS' },
                dkimVerdict: { status: 'PASS' },
                dmarcVerdict: { status: 'PASS' },
                processingTimeMillis: 500,
                timestamp: '2022-01-01T00:00:01Z',
                recipients: ['recipient@example.com'],
                action: {
                  type: 'Lambda',
                  topicArn:
                    'arn:aws:lambda:us-west-2:123456789012:function:my-function',
                },
                dmarcPolicy: '',
              },
            },
            eventVersion: '1.0',
            eventSource: 'aws:ses',
          },
        ],
      };

      const expectedOutputEvents: OutputEventDto[] = [
        {
          spam: true,
          virus: true,
          dns: true,
          mes: 'January',
          retrasado: false,
          emisor: 'sender',
          receptor: ['recipient'],
        },
      ];

      jest
        .spyOn(service, 'mapToMappedEvent')
        .mockResolvedValue(expectedOutputEvents);

      const result = await controller.eventMapper(inputEvent);

      expect(result).toEqual(expectedOutputEvents);
      expect(service.mapToMappedEvent).toHaveBeenCalledWith(inputEvent);
    });

    it('should throw HttpException when an error occurs', async () => {
      const inputEvent: InputEventDto = {
        Records: [
          {
            ses: {
              mail: {
                timestamp: '2022-01-01T00:00:00Z',
                source: 'sender@example.com',
                destination: ['recipient@example.com'],
                messageId: '',
                headersTruncated: false,
                headers: [],
                commonHeaders: {
                  from: ['sender@example.com'],
                  to: ['recipient@example.com'],
                  subject: 'Test Subject',
                  returnPath: '',
                  date: '',
                  messageId: '',
                },
              },
              receipt: {
                spamVerdict: { status: 'PASS' },
                virusVerdict: { status: 'PASS' },
                spfVerdict: { status: 'PASS' },
                dkimVerdict: { status: 'PASS' },
                dmarcVerdict: { status: 'PASS' },
                processingTimeMillis: 500,
                timestamp: '2022-01-01T00:00:01Z',
                recipients: ['recipient@example.com'],
                action: {
                  type: 'Lambda',
                  topicArn:
                    'arn:aws:lambda:us-west-2:123456789012:function:my-function',
                },
                dmarcPolicy: '',
              },
            },
            eventVersion: '1.0',
            eventSource: 'aws:ses',
          },
        ],
      };

      const errorMessage = 'Failed to map the provided JSON';
      jest
        .spyOn(service, 'mapToMappedEvent')
        .mockRejectedValue(new Error(errorMessage));

      await expect(controller.eventMapper(inputEvent)).rejects.toThrow(
        new HttpException(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR),
      );
    });
  });
});
