import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNumber, IsObject, IsString } from 'class-validator';

export class Status {
  @ApiProperty()
  @IsString()
  status: string;
}

export class ReceiptAction {
  @ApiProperty()
  @IsString()
  type: string;

  @ApiProperty()
  @IsString()
  topicArn: string;
}

export class SesReceipt {
  @ApiProperty()
  @IsString()
  timestamp: string;

  @ApiProperty()
  @IsNumber()
  processingTimeMillis: number;

  @ApiProperty()
  @IsArray()
  recipients: string[];

  @ApiProperty()
  @IsObject()
  spamVerdict: Status;

  @ApiProperty()
  @IsObject()
  virusVerdict: Status;

  @ApiProperty()
  @IsObject()
  spfVerdict: Status;

  @ApiProperty()
  @IsObject()
  dkimVerdict: Status;

  @ApiProperty()
  @IsObject()
  dmarcVerdict: Status;

  @ApiProperty()
  @IsString()
  dmarcPolicy: string;

  @ApiProperty()
  @IsObject()
  action: ReceiptAction;
}
