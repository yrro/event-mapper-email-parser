import { ApiProperty } from '@nestjs/swagger';
import { ValidateNested, IsObject } from 'class-validator';
import { SesReceipt } from './ses-receipt';
import { SesMail } from './ses-mail';

export class RecordSes {
  @ApiProperty({ type: SesReceipt })
  @IsObject()
  @ValidateNested()
  receipt: SesReceipt;

  @ApiProperty({ type: SesMail })
  @IsObject()
  @ValidateNested()
  mail: SesMail;
}
