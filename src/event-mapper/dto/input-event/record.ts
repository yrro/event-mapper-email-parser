import { ApiProperty } from '@nestjs/swagger';
import { IsObject, IsString, ValidateNested } from 'class-validator';
import { RecordSes } from './ses';
import { Type } from 'class-transformer';

export class Record {
  @ApiProperty()
  @IsString()
  eventVersion: string;

  @ApiProperty({ type: RecordSes })
  @IsObject()
  @ValidateNested()
  @Type(() => RecordSes)
  ses: RecordSes;

  @ApiProperty()
  @IsString()
  eventSource: string;
}
