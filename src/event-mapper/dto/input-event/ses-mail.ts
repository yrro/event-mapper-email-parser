import { IsArray, IsBoolean, IsObject, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MailHeaders {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  value: string;
}

export class MailCommonHeaders {
  @ApiProperty()
  @IsString()
  returnPath: string;

  @ApiProperty()
  @IsArray()
  @IsString()
  from: string[];

  @ApiProperty()
  @IsString()
  date: string;

  @ApiProperty()
  @IsArray()
  @IsString()
  to: string[];

  @ApiProperty()
  @IsString()
  messageId: string;

  @ApiProperty()
  @IsString()
  subject: string;
}

export class SesMail {
  @ApiProperty()
  @IsString()
  timestamp: string;

  @ApiProperty()
  @IsString()
  source: string;

  @ApiProperty()
  @IsString()
  messageId: string;

  @ApiProperty()
  @IsArray()
  destination: string[];

  @ApiProperty()
  @IsBoolean()
  headersTruncated: boolean;

  @ApiProperty()
  @IsObject()
  @IsArray()
  headers: MailHeaders[];

  @ApiProperty()
  @IsObject()
  commonHeaders: MailCommonHeaders;
}
