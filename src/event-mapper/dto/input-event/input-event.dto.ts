import { IsArray, ValidateNested } from 'class-validator';
import { Record } from './record';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

// Parent DTO - It contains all the nested objects
export class InputEventDto {
  @ApiProperty({ type: [Record] })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Record)
  Records: Record[];
}
