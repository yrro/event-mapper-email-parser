import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsString } from 'class-validator';

export class OutputEventDto {
  @ApiProperty()
  @IsBoolean()
  spam: boolean;

  @ApiProperty()
  @IsBoolean()
  virus: boolean;

  @ApiProperty()
  @IsBoolean()
  dns: boolean;

  @ApiProperty()
  @IsString()
  mes: string;

  @ApiProperty()
  @IsBoolean()
  retrasado: boolean;

  @ApiProperty()
  @IsString()
  emisor: string;

  @ApiProperty()
  @IsArray()
  receptor: string[];
}
