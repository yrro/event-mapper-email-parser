import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { InputEventDto } from '../dto/input-event/input-event.dto';
import { OutputEventDto } from '../dto/output-event/output-event.dto';
import { Record } from '../dto/input-event/record';
import { PrismaService } from '../../prisma/prisma.service';
import { monthNames } from '../../helpers/types/month-names';

// Constant to represent the pass status for verdicts
const PASS = 'PASS';

@Injectable()
export class EventMapperService {
  constructor(private readonly _prismaService: PrismaService) {}

  /**
   * Maps an input event to an array of output events.
   * @param inputEvent The input event data transfer object.
   * @returns An array of mapped output event data transfer objects.
   */
  
  async mapToMappedEvent(inputEvent: InputEventDto): Promise<OutputEventDto[]> {
    try {
      // Mapping each record in the input event to an output event
      const outputEvents = inputEvent.Records.map((record: Record) => {
        const mail = record.ses.mail;
        const receipt = record.ses.receipt;

        // Determine the status of spam, virus, and DNS verdicts
        const spam = receipt.spamVerdict.status === PASS;
        const virus = receipt.virusVerdict.status === PASS;
        const dns =
          receipt.spfVerdict.status === PASS &&
          receipt.dkimVerdict.status === PASS &&
          receipt.dmarcVerdict.status === PASS;
        
        // Extract month name from the timestamp
        const date = new Date(mail.timestamp);
        const mes = monthNames[date.getUTCMonth()];

        // Determine if the email processing was delayed
        const retrasado = receipt.processingTimeMillis > 1000;
        
        // Extract the email source and destination usernames
        const emisor = mail.source.split('@')[0];
        const receptor = mail.destination.map((dest) => dest.split('@')[0]);

        // Create an instance of OutputEventDto with mapped values
        const mappedInputEvent = plainToInstance(OutputEventDto, {
          spam,
          virus,
          dns,
          mes,
          retrasado,
          emisor,
          receptor,
        });
        return mappedInputEvent;
      });

      // Save the mapped events to the database
      await this._prismaService.mappedJson.createMany({
        data: outputEvents.map((event: OutputEventDto) => ({
          spam: event.spam,
          virus: event.virus,
          dns: event.dns,
          mes: event.mes,
          retrasado: event.retrasado,
          emisor: event.emisor,
          receptor: event.receptor,
        })),
      });

      // Return the mapped output events
      return outputEvents;
    } catch (error) {
      // Handle errors by throwing an internal server error exception
      throw new InternalServerErrorException(
        'Failed to map input JSON.',
        error,
      );
    }
  }
}
