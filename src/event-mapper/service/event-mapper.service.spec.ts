import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../../prisma/prisma.service';
import { EventMapperService } from './event-mapper.service';
import { InternalServerErrorException } from '@nestjs/common';
import { InputEventDto } from '../dto/input-event/input-event.dto';
import { OutputEventDto } from '../dto/output-event/output-event.dto';
import { MailCommonHeaders } from '../dto/input-event/ses-mail';
import { ReceiptAction } from '../dto/input-event/ses-receipt';

describe('EventMapperService', () => {
  let service: EventMapperService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EventMapperService, PrismaService],
    }).compile();

    service = module.get<EventMapperService>(EventMapperService);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('mapToMappedEvent', () => {
    it('should map input event to output events correctly', async () => {
      const inputEvent: InputEventDto = {
        Records: [
          {
            ses: {
              mail: {
                timestamp: '2022-01-01T00:00:00Z',
                source: 'sender@example.com',
                destination: ['recipient@example.com'],
                messageId: '',
                headersTruncated: false,
                headers: [],
                commonHeaders: new MailCommonHeaders(),
              },
              receipt: {
                spamVerdict: { status: 'PASS' },
                virusVerdict: { status: 'PASS' },
                spfVerdict: { status: 'PASS' },
                dkimVerdict: { status: 'PASS' },
                dmarcVerdict: { status: 'PASS' },
                processingTimeMillis: 500,
                timestamp: '',
                recipients: [],
                dmarcPolicy: '',
                action: new ReceiptAction(),
              },
            },
            eventVersion: '',
            eventSource: '',
          },
        ],
      };

      const expectedOutputEvents: OutputEventDto[] = [
        {
          spam: true,
          virus: true,
          dns: true,
          mes: 'January',
          retrasado: false,
          emisor: 'sender',
          receptor: ['recipient'],
        },
      ];

      jest
        .spyOn(prismaService.mappedJson, 'createMany')
        .mockResolvedValue(null);

      const result = await service.mapToMappedEvent(inputEvent);

      expect(result).toEqual(expectedOutputEvents);
      expect(prismaService.mappedJson.createMany).toHaveBeenCalledWith({
        data: expectedOutputEvents,
      });
    });

    it('should throw InternalServerErrorException when mapping fails', async () => {
      const inputEvent: InputEventDto = {
        Records: [
          {
            ses: {
              mail: {
                timestamp: '2022-01-01T00:00:00Z',
                source: 'sender@example.com',
                destination: ['recipient@example.com'],
                messageId: '',
                headersTruncated: false,
                headers: [],
                commonHeaders: new MailCommonHeaders(),
              },
              receipt: {
                spamVerdict: { status: 'PASS' },
                virusVerdict: { status: 'PASS' },
                spfVerdict: { status: 'PASS' },
                dkimVerdict: { status: 'PASS' },
                dmarcVerdict: { status: 'FAIL' }, // Simulating mapping failure
                processingTimeMillis: 500,
                timestamp: '',
                recipients: [],
                dmarcPolicy: '',
                action: new ReceiptAction(),
              },
            },
            eventVersion: '',
            eventSource: '',
          },
        ],
      };

      jest
        .spyOn(prismaService.mappedJson, 'createMany')
        .mockImplementation(() => {
          throw new Error();
        });

      await expect(service.mapToMappedEvent(inputEvent)).rejects.toThrow(
        new InternalServerErrorException('Failed to map input JSON.'),
      );
    });
  });
});
