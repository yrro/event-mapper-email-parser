import { Test, TestingModule } from '@nestjs/testing';
import { EventMapperModule } from './event-mapper.module';
import { EventMapperController } from './controller/event-mapper.controller';
import { EventMapperService } from './service/event-mapper.service';
import { PrismaService } from '../prisma/prisma.service';

describe('EventMapperModule', () => {
  let module: EventMapperModule;
  let controller: EventMapperController;
  let service: EventMapperService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const testingModule: TestingModule = await Test.createTestingModule({
      imports: [EventMapperModule],
    }).compile();

    module = testingModule.get<EventMapperModule>(EventMapperModule);
    controller = testingModule.get<EventMapperController>(
      EventMapperController,
    );
    service = testingModule.get<EventMapperService>(EventMapperService);
    prismaService = testingModule.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(module).toBeDefined();
  });

  it('should have EventMapperController', () => {
    expect(controller).toBeDefined();
  });

  it('should have EventMapperService', () => {
    expect(service).toBeDefined();
  });

  it('should have PrismaService', () => {
    expect(prismaService).toBeDefined();
  });
});
