import { Module } from '@nestjs/common';
import { EventMapperController } from './controller/event-mapper.controller';
import { PrismaModule } from '../prisma/prisma.module';
import { EventMapperService } from './service/event-mapper.service';
import { PrismaService } from '../prisma/prisma.service';

@Module({
  controllers: [EventMapperController],
  providers: [EventMapperService, PrismaService],
  imports: [PrismaModule],
})
export class EventMapperModule {}
