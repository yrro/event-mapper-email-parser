import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from './app.module';
import { ValidationPipe } from './validation.pipe';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { INestApplication } from '@nestjs/common';

describe('AppModule', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.setGlobalPrefix('api/v1');
    app.useGlobalPipes(new ValidationPipe());

    const config = new DocumentBuilder()
      .setTitle('Mapping Input JSON into desired Output - Parsing Email')
      .setDescription('API for handling JSON input/output and Parsing Email.')
      .setVersion('0.1')
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('docs', app, document);

    app.enableCors();

    await app.init();
  });

  it('should use global validation pipe', () => {
    expect(app).toBeDefined();
  });

  it('should set up Swagger documentation', () => {});

  it('should enable CORS', () => {
    expect(app).toBeDefined();
  });

  afterEach(async () => {
    await app.close();
  });
});
