import { ValidationPipe } from './validation.pipe';

describe('ValidationPipe', () => {
  let validationPipe: ValidationPipe;

  beforeEach(() => {
    validationPipe = new ValidationPipe();
  });

  it('should build error messages correctly', () => {
    const errors = [
      {
        property: 'mockValue',
        constraints: {
          isString: 'mockValue must be a string',
        },
      },
    ];

    const errorMessage = validationPipe['buildError'](errors);

    expect(errorMessage).toBe('mockValue must be a string');
  });

  it('should correctly identify types to validate', () => {
    const typesToValidate = [String, Boolean, Number, Array, Object];
    const invalidType = class MockType {};

    for (const type of typesToValidate) {
      expect(validationPipe['toValidate'](type)).toBe(false);
    }

    expect(validationPipe['toValidate'](invalidType)).toBe(true);
  });
});
