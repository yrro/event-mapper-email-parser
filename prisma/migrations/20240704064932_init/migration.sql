-- CreateTable
CREATE TABLE "mappedJson" (
    "id" SERIAL NOT NULL,
    "spam" BOOLEAN NOT NULL,
    "virus" BOOLEAN NOT NULL,
    "dns" BOOLEAN NOT NULL,
    "mes" TEXT NOT NULL,
    "retrasado" BOOLEAN NOT NULL,
    "emisor" TEXT NOT NULL,
    "receptor" TEXT[],

    CONSTRAINT "mappedJson_pkey" PRIMARY KEY ("id")
);
