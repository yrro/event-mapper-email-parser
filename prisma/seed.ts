import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function upsertMappedJson(data) {
  const { emisor } = data;
  
  // Query the existing record to get its id if it exists
  const existingRecord = await prisma.mappedJson.findFirst({
    where: { emisor }
  });

  if (existingRecord) {
    // Update the existing record
    return prisma.mappedJson.upsert({
      where: { id: existingRecord.id },
      update: data,
      create: data
    });
  } else {
    // Create a new record
    return prisma.mappedJson.create({
      data
    });
  }
}

const emailData1 = {
  data: {
    subject: 'Mocked Email 1',
    body: 'This is a mocked email content for Email 1.',
    sender: 'sender@example.com',
    recipients: ['recipient1@example.com', 'recipient2@example.com'],
    attachments: ['attachment1.pdf', 'attachment2.docx'],
  },
};

const emailData2 = {
  data: {
    subject: 'Mocked Email 2',
    body: 'This is a mocked email content for Email 2.',
    sender: 'another.sender@example.com',
    recipients: ['another.recipient@example.com'],
    attachments: ['another_attachment.pdf'],
  },
};


async function main() {
  const event1 = await upsertMappedJson({
    spam: true,
    virus: true,
    dns: true,
    mes: 'July',
    retrasado: false,
    emisor: 'user1',
    receptor: ['user2']
  });

  const event2 = await upsertMappedJson({
    spam: false,
    virus: true,
    dns: false,
    mes: 'August',
    retrasado: true,
    emisor: 'user3',
    receptor: ['user4']
  });

  console.log(event1, event2);

  const email1 = await prisma.email.upsert({
    where: { id: 1 }, 
    update: emailData1,
    create: emailData1,
  });

  const email2 = await prisma.email.upsert({
    where: { id: 2 }, 
    update: emailData2,
    create: emailData2,
  });

  console.log('Seeded emails:', email1, email2);
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
